Getting started with this project:

# Install
1. Install [Ruby](https://www.ruby-lang.org/en/documentation/installation/)
2. Install [Compass](http://compass-style.org)
  - `$ gem install compass`
3. Install [Autoprefixer](https://github.com/postcss/autoprefixer)
  - `$ gem install autoprefixer-rails`
  - Optionally disable Autoprefixer integration by commenting out the appropriate lines in `config/compass.rb`.
    - This would result in a less compatible output but does not affect the main styles.
  
# Make Changes
1. Change directory in your command line to the directory containing this file.
2. `compass watch`
  - Ctrl-C to close the program.
  
# Notes
- To make the output css more readable, change this line of `config/compass.rb`:
  - `output_style = :compressed`
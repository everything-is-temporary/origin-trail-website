function linkClick(identifier, link){
  gtag('event', 'click', {
    event_category: 'social_links',
    event_label: identifier + "." + link.name
  });
}

function downloadButtonClick(downloadType){
  gtag('event', 'click', {
    event_category: 'downloads',
    event_label: 'download_'+downloadType
  });
}
